package restassuredTest;

import org.testng.annotations.Test;
import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;
import org.testng.annotations.Test;


public class Demo1_GET_Request {
	
	@Test
	public void getWeatherDetails() {
		
		given()
		.when()
			.get("http://restapi.demoqa.com/utilities/weather/city/hyderabad")
		.then()
				.statusCode(200)
				.statusLine("HTTP/1.1 200 OK")
				.assertThat().body("City", equalTo("Hyderabad"))
				/* aqui podemos poner cualquier otra assercion*/
//				.assertThat().body("Humidity", equalTo("88 Percent"))
				.header("Content-Type", "application/json2");
		
		
	}
	
	

}
