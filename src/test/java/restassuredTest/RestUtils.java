package restassuredTest;

import org.apache.commons.lang3.RandomStringUtils;

public class RestUtils {
	
	/*
	 * Metodos para generar data
	 * */
	
	public static String getFirstName() {
		String cadenaGenerada = RandomStringUtils.randomAlphabetic(1);
		return ("wil"+cadenaGenerada);
	}
	
	public static String getLastName() {
		String cadenaGenerada = RandomStringUtils.randomAlphabetic(1);
		return ("Angel"+cadenaGenerada);
	}
	
	public static String getUserName() {
		String cadenaGenerada = RandomStringUtils.randomAlphabetic(3);
		return ("UserName"+cadenaGenerada);
	}
	
	public static String getPassword() {
		String cadenaGenerada = RandomStringUtils.randomNumeric(3);
		return ("Password"+cadenaGenerada);
	}
	
	public static String getEmail() {
		String cadenaGenerada = RandomStringUtils.randomAlphabetic(3);
		return (cadenaGenerada+"-gmail.com");
	}
	
	public static String getEmpName() {
		String cadenaGenerada = RandomStringUtils.randomAlphabetic(3);
		return ("EmployeeName"+cadenaGenerada);
	}
	
	public static String getEmpSal() {
		String cadenaGenerada = RandomStringUtils.randomNumeric(5);
		return (cadenaGenerada);
	}
	
	
	public static String getEmpAge() {
		String cadenaGenerada = RandomStringUtils.randomNumeric(2);
		return (cadenaGenerada);
	}

}
